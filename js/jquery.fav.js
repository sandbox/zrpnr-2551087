(function($) {

  $.fn.favSetActive = function(favId, favList) {
    var $list,
        $link,
        $addLinks;

    $list = $('.fav-list');
    $link = $(this).find('a');

    $list.find('li a').not($link).removeClass('active');
    $link.addClass('active');

    $addLinks = $('.fav-link');

    $addLinks.each(function() {
      var $addLink,
          linkData,
          addLink,
          base;

      $addLink = $(this);
      linkData = $addLink.data();
      addLink  = '/fav/ajax/add/' + linkData.id + '/' + linkData.fav + '/' + favId;
      base     = $addLink.attr('id');

      // Check if node is added to current fav.
      if (favList.indexOf(String(linkData.id)) > -1) {
        $addLink.addClass('added');
      }
      else {
        $addLink.removeClass('added');
      }

      $addLink.attr({'href': addLink});

      // Drupal stores its own references to use-ajax links.
      Drupal.ajax[base].url = addLink;
      Drupal.ajax[base].options.url = addLink;
    });
  };

  $.fn.favToggle = function() {

    $(this).toggleClass('added');
  }

})(jQuery);