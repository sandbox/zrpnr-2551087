<?php

/**
 * Implements hook_views_default_views().
 */
function fav_views_default_views() {
  $views = array();

  // Load in all the files from the /views folder
  $files = file_scan_directory(drupal_get_path('module', 'fav'). '/views', '/.view/');
  foreach ($files as $filepath => $file) {

    require $filepath;

    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }

  return $views;
}
